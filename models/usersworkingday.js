'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UsersWorkingDay extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.User, {foreignKey: 'user_id'})
      this.belongsTo(models.WorkingDay, {foreignKey: 'working_day_id'})
    }
  }
  UsersWorkingDay.init({
    user_id: DataTypes.INTEGER,
    working_day_id: DataTypes.INTEGER
  }, {
    sequelize,
    tableName: 'users_working_days',
    modelName: 'UsersWorkingDay',
    underscored: true,
  });
  return UsersWorkingDay;
};