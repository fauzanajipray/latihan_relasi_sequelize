'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class WorkingDay extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      WorkingDay.belongsToMany(models.User, {through: 'users_working_days', foreignKey: 'workingDayId', as: 'employes'})
    }
  }
  WorkingDay.init({
    week_day: DataTypes.STRING,
    working_date: DataTypes.DATE,
    is_working: DataTypes.BOOLEAN
  }, {
    sequelize,
    tableName: 'working_days',
    modelName: 'WorkingDay',
    underscored: true,
  });
  return WorkingDay;
};