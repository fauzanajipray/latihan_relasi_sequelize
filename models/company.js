'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Company extends Model {

    static associate(models) {
      this.hasMany(models.User, {as: 'users'})
    }
  }
  Company.init({
    name: DataTypes.STRING,
  }, {
    sequelize,
    tableName: 'companies',
    modelName: 'Company',
    underscored: true,
  });

  return Company;
};