'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    
    static associate(models) {
      this.belongsTo(models.Company, {foreignKey: 'company_id', as: 'company'})
      this.belongsToMany(models.WorkingDay, {through: 'users_working_days', foreignKey: 'user_id', as: 'working_days'}) // many to many
    }
  }
  User.init({
    email: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    tableName: 'users',
    modelName: 'User',
    underscored: true,
  });

  return User;
};