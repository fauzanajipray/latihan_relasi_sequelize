// Ini coba2 kalau pakai express guys

const express = require('express');
const { Company, User, WorkingDay } = require('./models');

const app = express();
const port = 3000;
const morgan = require('morgan');

app.use(morgan('dev'));

app.get('/test/:id', (req, res) => {
    const id = req.params.id;
    Company.findOne({
        where: {
            id: id
        },
        include: [{
            model: User,
            as: 'users',
            // where: {
            //     id: 8
            // },
            include: [{
                model: WorkingDay,
                as: 'working_days',
            }]
        }]
    }).then(company => {
        res.json(
            company.toJSON()
        )
    })
})


app.listen(port, () => console.log(`Example app listening on port ${port}!`));