'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('working_days', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      week_day: {
        type: Sequelize.STRING
      },
      working_date: {
        type: Sequelize.DATE
      },
      is_working: {
        type: Sequelize.BOOLEAN
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('working_days');
  }
};