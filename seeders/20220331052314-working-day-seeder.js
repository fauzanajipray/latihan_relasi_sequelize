'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    
    const workingDay = [
      {
        week_day: 'Senin',
        working_date: '2020-02-01',
        is_working: true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        week_day: 'Selasa',
        working_date: '2020-02-02',
        is_working: true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        week_day: 'Rabu',
        working_date: '2020-02-03',
        is_working: true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        week_day: 'Kamis',
        working_date: '2020-02-04',
        is_working: true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        week_day: 'Jumat',
        working_date: '2020-02-05',
        is_working: true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        week_day: 'Sabtu',
        working_date: '2020-02-06',
        is_working: true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        week_day: 'Minggu',
        working_date: '2020-02-07',
        is_working: true,
        created_at: new Date(),
        updated_at: new Date()
      }
    ];

    return queryInterface.bulkInsert('working_days', workingDay, {} );
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
