'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    
    const data = [];
    for (let i = 1; i < 20; i++) {
        let userId = Math.floor(Math.random() * 10) + 1;
        let workingDayId = Math.floor(Math.random() * 7) + 1;
        data.push({
          user_id: userId,
          working_day_id: workingDayId,
          created_at: new Date(),
          updated_at: new Date()
        })
    }

    console.log(data);

    await queryInterface.bulkInsert('users_working_days', data, {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
