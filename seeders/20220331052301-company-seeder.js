'use strict';
const { faker } = require('@faker-js/faker')
faker.locale = 'id_ID'

module.exports = {
  async up (queryInterface, Sequelize) {
    
    const data = [];
    for (let i = 0; i < 5; i++) {
      data.push({
        name: faker.company.companyName(),
        created_at: new Date(),
        updated_at: new Date()
      })
    }
    console.log(data);
    await queryInterface.bulkInsert('companies', data, {});

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
