'use strict';
const { faker } = require('@faker-js/faker');

module.exports = {
  async up (queryInterface, Sequelize) {
    
    const data = [];
    for (let i = 0; i < 5; i++) {
      let firstName = faker.name.firstName();
      let lastName = faker.name.lastName();
      let email = faker.internet.email(firstName, lastName);
      let companyId = Math.floor(Math.random() * 5) + 1;
      data.push({
        email: email,
        first_name: firstName,
        last_name: lastName,
        company_id: companyId,
        created_at: new Date(),
        updated_at: new Date()
      })
    }
    console.log(data);
    await queryInterface.bulkInsert('users', data, {});

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
