## Catatan 

- instal sequelize
``` npm install sequelize pg ```

- init folder
``` npx sequelize-cli init ```

- Create Database
``` npx sequelize-cli db:create ```

- create tabel working_day
``` npx sequelize-cli model:create --name=WorkingDay --attributes=week_day:string,working_date:date,is_working:boolean --underscored ```

- create table user
``` npx sequelize-cli model:create --name=User --attributes=email:string,first_name:string,last_name:string,company_id:integer --underscored ``` 

- create table company
``` npx sequelize-cli model:create --name=Company --attributes=name:string --underscored ```

- create tabel users_working_day
``` npx sequelize-cli model:create --name=UsersWorkingDay --attributes=user_id:integer,working_day_id:integer --underscored ```

- create seeder company
``` npx sequelize-cli seed:create --name=company-seeder ```

- create seeder user
``` npx sequelize-cli seed:create --name=user-seeder ```

- create seeder working_day
``` npx sequelize-cli seed:create --name=working-day-seeder ```

- create seeder users_working_day
``` npx sequelize-cli seed:create --name=users-working-day-seeder ```
