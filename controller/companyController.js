const { Company } = require('../models');

module.exports = {
    // Create company
    create: () => {
        Company.create({
            name: 'PT. Gojek'
        }).then(company => {
            console.log('Company berhasil dibuat');
            console.log(company.toJSON());
        })
    },
    // Mengupdate company
    update: () => {
        const query = { where: { id: 1 } }
        Company.update({
            id: 1
        }, query )
        .then(() => {
            console.log("Company berhasil diupdate")
            process.exit()
        }).catch(err => {
            console.error("Gagal mengupdate company!")
        })
    },
    // Mencari semua data company
    findAll: () => {
        const data = []
        Company.findAll().then(companies => {
            companies.forEach(company => {
                data.push(company.toJSON())
            })
            console.log(data);
            process.exit()
        })
    },
    // Mencari company berdasarkan id
    findOne: (id) => {
        Company.findOne({
            where: {
                id: id
            },
            include: ['users']
        }).then(company => {
            console.log(company.toJSON());
            process.exit()
        })
    },
    // Menghapus company berdasarkan id 
    destroy: (id) => {
        Company.destroy({
            where: {
                id: id
            }
        }).then(() => {
            console.log("Company berhasil dihapus")
            process.exit()
        })
    },
    // Untuk find one company yang memiliki user yang memiliki working day
    findOneWithIncludeInInclude: (id) => {
        Company.findOne({
            where: {
                id: id
            },
            include: [{
                model: User,
                as: 'users',
                // where: {
                //     id: 8
                // },
                include: [{
                    model: WorkingDay,
                    as: 'working_days',
                }]
            }]
        }).then(company => {
            console.log(company.toJSON());
        })
    }
}

