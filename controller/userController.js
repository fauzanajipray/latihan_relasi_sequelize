const { User } = require('../models')

module.exports = {
    // Create user
    create: () => {
        User.create({
            email: 'example5@gmail.com',
            first_name: 'Toni',
            last_name: 'Chopper',
            company_id: 1
        }).then(user => {
            console.log('User berhasil dibuat');
            console.log(user.toJSON());
        })
    },
    // Mengupdate user
    update: () => {
        const query = { where: { id: 1 } }
        User.update({
            id: 1
        }, query )
        .then(() => {
            console.log("User berhasil diupdate")
            process.exit()
        }).catch(err => {
            console.error("Gagal mengupdate user!")
        })
    },
    // Mencari semua data user
    findAll: () => {
        const data = []
        User.findAll({ 
            include: ['company', 'working_days'] 
        }).then(users => {
            users.forEach(user => {
                data.push(user.toJSON())
            })
            data.map(user => {
                delete user.CompanyId
                delete user.createdAt
                delete user.password
            })
            console.log(data);
            process.exit()
        })
    },
    // Mencari user berdasarkan id
    findOne: (id) => {
        User.findOne({
            where: {
                id: id
            },
            include: ['company', 'working_days'] 
        }).then(user => {
            console.log(user.toJSON())
            process.exit()
        })
    },
    // Menghapus user berdasarkan id
    destroy: (id) => {
        User.destroy({
            where: {
                id: id
            }
        }).then(() => {
            console.log("User berhasil dihapus")
            process.exit()
        })
    }
}