const { WorkingDay } = require('../models');

module.exports = {
    // Create working day
    findAll: () => {
        const data = []
        WorkingDay.findAll({ 
            include: ['employes'] 
        }).then(workingDays => {
            workingDays.forEach(workingDay => {
                data.push(workingDay.toJSON())
            })
            // data.map(workingDay => {
            //     delete workingDay.CompanyId
            //     delete workingDay.createdAt
            // })
            console.log(data);
            process.exit()
        })
    },
    // Mencari user berdasarkan id
    findOne: (id) => {
        WorkingDay.findOne({
            where: {
                id: id
            },
            include: ['employes'] 
        }).then(workingDay => {
            console.log(workingDay.toJSON())
            process.exit()
        })
    }
}
